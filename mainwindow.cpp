#include "mainwindow.hpp"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(SqlUtil::getInstance(), SIGNAL(updateUI(QString)), this, SLOT(receiveMsg(QString)));
    this->setFixedSize(QSize(1180, 840));
    path_ = "单击右侧选择按钮选择您的路径";
    server_ = new Server(38080, "./default/");
    inService_ = false;
    ui->stopServerBtn->setEnabled(false);
}

MainWindow::~MainWindow(){
    delete ui;
    delete server_;
}

void MainWindow::on_selectPathBtn_clicked(){
    QString tempPath = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                         QDir::currentPath(),
                                                         QFileDialog::ShowDirsOnly
                                                         | QFileDialog::DontResolveSymlinks);
    if (!tempPath.isEmpty()){ // 获取路径成功
        if (path_ != tempPath){ // 路径被改变
            path_ = tempPath;
            server_->reset(38080, path_);
        }
    }
    SPDLOG_INFO("path_ : {}", path_.toStdString());
    ui->showPathLE->setText(path_);
}

void MainWindow::on_startServerBtn_clicked(){
    if (path_ == "单击右侧选择按钮选择您的路径"){
        QMessageBox::warning(nullptr, "注意", "请先选择路径");
        return;
    }
    inService_ = true;

    server_->resume();
    ui->startServerBtn->setEnabled(false);
    ui->stopServerBtn->setEnabled(true);
    ui->selectPathBtn->setEnabled(false);

}

void MainWindow::on_stopServerBtn_clicked(){
    inService_ = false;

    server_->pause();
    ui->startServerBtn->setEnabled(true);
    ui->stopServerBtn->setEnabled(false);
    ui->selectPathBtn->setEnabled(true);
}
