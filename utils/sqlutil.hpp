#ifndef SQLUTIL_HPP
#define SQLUTIL_HPP
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QObject>

class SqlUtil: public QObject
{
    Q_OBJECT
private:
    QSqlDatabase dbConn;
    static SqlUtil *instance;
    QSqlQuery query;
    bool isOpen;

protected:
    SqlUtil();
public:
    ~SqlUtil();
    static SqlUtil *getInstance();

    /**
     * @brief config    配置函数
     * @param hostname  数据库主机IP
     * @param username  数据库账号
     * @param password  数据库密码
     * @param dbname    数据库名
     * @return          配置成功?
     */
    bool config(QString hostname="localhost", QString username="root", QString password="123456", QString dbname="student_information");

signals:
    void updateUI(QString msg);
};

#endif // SQLUTIL_HPP
