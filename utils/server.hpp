#ifndef SERVER_HPP
#define SERVER_HPP
#include <QTcpServer>
#include "utils/recvfile.hpp"
#include <QMessageBox>

class Server : public QObject
{
    Q_OBJECT
private:
    QTcpServer* server_;
    QString path_;
public:
    /**
     * @brief Server
     * @param port      默认端口, 38080
     */
    Server(int port = 38080, QString path = "./");

    /**
     * @brief resume 继续接受请求
     * @return
     */
    bool resume();

    /**
     * @brief pause 停止接收请求
     * @return
     */
    bool pause();

    void setPath(const QString &path);

    bool reset(int port=38080, QString path="./");
};

#endif // SERVER_HPP
