#ifndef RECVFILE_HPP
#define RECVFILE_HPP

#include <QThread>
#include <QTcpSocket>
#include <QFile>
#include <QDir>
#include "spdlog/spdlog.h"

class RecvFile : public QThread
{
    Q_OBJECT
private:
    QTcpSocket* socket_;
    QString path_;
    QString full_path_;
public:
    RecvFile(QTcpSocket *socket, QString path);

    void run()override;
    void setPath(const QString &path);
    void setSocket(QTcpSocket *socket);

    QString full_path() const;

signals:
    /**
     * @brief over
     * @param statusCode 0 正常结束
     *                  -1 文件已存在
     *                  -2 打开文件失败
     */
    void over(int statusCode);
};

#endif // RECVFILE_HPP
