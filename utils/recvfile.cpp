#include "recvfile.hpp"


QString RecvFile::full_path() const{
    return full_path_;
}

RecvFile::RecvFile(QTcpSocket *socket, QString path):socket_(socket),path_(path){
    connect(socket_, &QTcpSocket::disconnected, [](){
        qDebug() << "已断开连接";
    });
}

void RecvFile::setPath(const QString &path){
    path_ = path;
}

void RecvFile::setSocket(QTcpSocket *socket){
    socket_ = socket;
}

void RecvFile::run(){
    if (path_.isEmpty()){
        path_ = "./files/";
    }
    QDir *dir = new QDir(path_);
    if (!dir->exists()){
        dir->mkpath(path_);
        qDebug() << path_ << " : 路径不存在,已创建";
    }
    full_path_ = dir->path() + "/";
    delete dir;

    QFile* file = new QFile;
    long fileSize = 0;
    long receiveSize = 0;
    bool head = true;
    QByteArray buf;

    connect(socket_, &QTcpSocket::readyRead, [=,&buf,&head,&receiveSize,&fileSize](){
        buf += socket_->readAll();
        // 头部信息
        if (head){
            head = false;
            QString fileName = QString(buf).section("#br#", 0, 0);
            fileSize = QString(buf).section("#br#", 1, 1).toInt();
            buf.remove(0, (fileName + QString("#br#%1#br#").arg(fileSize)).size());
            //buf = QString(buf).section("#br#", 2).toUtf8().data();
            SPDLOG_INFO("buf is {}", buf);
            SPDLOG_INFO("buf'size is {}", buf.size());

            SPDLOG_INFO("headInfo: fileName is {}, fileSize is {}", fileName.toStdString(), fileSize);
            file->setFileName(path_ + fileName);

            full_path_ += fileName;
            SPDLOG_INFO(" filePath : {}", full_path_.toStdString());

            if(file->exists()){
                SPDLOG_WARN("file exists");
                socket_->write("-1");
                socket_->flush();
                file->close();
                file->deleteLater();
                socket_->disconnectFromHost();
                socket_->close();
                socket_->deleteLater();
                emit over(-1); // 文件存在-1
                return;
            }

            if (!file->open(QFile::WriteOnly | QFile::Append)){
                SPDLOG_ERROR("打开文件 {} 失败", fileName.toStdString());
                socket_->write("-2");
                socket_->flush();

                file->close();
                file->deleteLater();
                socket_->disconnectFromHost();
                socket_->close();
                socket_->deleteLater();
                emit over(-2); // 调用结束, 结束线程
            }else {
                qDebug() << fileName << " 文件打开成功";
            }
        }else {
            qint64 len = file->write(buf, buf.size());
            buf.clear();
            receiveSize += len;
            if (len == 0 || receiveSize >= fileSize){
                SPDLOG_INFO("接收文件成功");
                socket_->write("0");
                socket_->flush();
                file->close();
                file->deleteLater();
                socket_->disconnectFromHost();
                socket_->close();
                socket_->deleteLater();
                emit over(0);
            }
        }
    });

    exec();
}
