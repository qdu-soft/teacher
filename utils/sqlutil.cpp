#include "sqlutil.hpp"

SqlUtil *SqlUtil::instance = nullptr;

SqlUtil::SqlUtil(){
    dbConn = QSqlDatabase::addDatabase("QMYSQL");
}

SqlUtil::~SqlUtil(){
    dbConn.close();
    delete instance;
}

SqlUtil* SqlUtil::getInstance(){
    if (instance == nullptr){
        instance = new SqlUtil();
    }
    return instance;
}

bool SqlUtil::config(QString hostname, QString username, QString password, QString dbname){
    dbConn.setHostName(hostname);
    dbConn.setUserName(username);
    dbConn.setPassword(password);
    dbConn.setPort(3306);
    dbConn.setDatabaseName(dbname);
    dbConn.setConnectOptions("MYSQL_OPT_CONNECT_TIMEOUT=5");
    isOpen = dbConn.open();
    query = QSqlQuery(dbConn);      // 打开连接
    return isOpen;
}
