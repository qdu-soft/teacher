#include "server.hpp"

void Server::setPath(const QString &path){
    path_ = path;
}

bool Server::reset(int port, QString path){
    path_ = path;
    server_->listen(QHostAddress::Any, ushort(port));
    server_->pauseAccepting(); // 暂停接收请求

    return true;
}

Server::Server(int port, QString path){
    server_ = new QTcpServer(this);
    reset(port, path);

    QObject::connect(server_, &QTcpServer::newConnection, [=](){
        QTcpSocket *socket = server_->nextPendingConnection();
        SPDLOG_INFO("connected");
        RecvFile* recvFile = new RecvFile(socket, path_ + "/");
        QObject::connect(recvFile, &RecvFile::finished, [=](){
            recvFile->deleteLater();
        });
        QObject::connect(recvFile, &RecvFile::over, [=](int statusCode){
            SPDLOG_INFO("statusCode is {}", statusCode);
            /*
            switch (statusCode) {
            case 0:
                QMessageBox::warning(nullptr, "信息", "传输文件完成");
                break;
            case -1:
                QMessageBox::warning(nullptr, "注意", "文件已存在");
                break;
            case -2:
                QMessageBox::warning(nullptr, "注意", "文件打开失败");
                break;
            }
            */
            recvFile->exit();
            recvFile->wait();
            SPDLOG_INFO("disConnected");
        });
        recvFile->start();
    });
}

bool Server::resume(){
    SPDLOG_INFO("继续接收请求");
    server_->resumeAccepting();
    return true;
}

bool Server::pause(){
    SPDLOG_INFO("停止接收请求");
    server_->pauseAccepting();
    return true;
}
