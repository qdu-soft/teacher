#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QMessageBox>
#include "utils/sqlutil.hpp"
#include <QDir>
#include <QFileDialog>
#include <QDebug>
#include "utils/server.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void receiveMsg(QString msg){
        QMessageBox::information(nullptr, "注意", msg);
    }
private slots:
    void on_selectPathBtn_clicked();

    void on_startServerBtn_clicked();

    void on_stopServerBtn_clicked();

private:
    Ui::MainWindow *ui;
    QString path_;
    bool inService_;
    Server* server_;
};

#endif // MAINWINDOW_HPP
